# Code Challenge Review

## Welcome

Thank you for participating in our code challenge review.  Please follow the steps below to continue.


Should you have any questions or issues please contact us at [nathan.d.kincaid@disney.com](mailto:nathan.d.kincaid@disney.com)

### Steps To Get Started

1. Sign up or, if you already have an account, log in at [gitlab](https://gitlab.com/users/sign_in)

2. Email your gitlab username to [nathan.d.kincaid@disney.com](mailto:nathan.d.kincaid@disney.com)

3. You will be emailed when you're added to the code challenge repository and when you are assigned a merge request.

4. Follow the merge request email instructions and be sure to review the README of the repo you've been assigned.


### That's it!  Good luck. 

